# Estudo para novo formulario de contratação

Foi questionada e falta de um formulario contendo informações sobre a Novatics e as vagas disponiveis durante o processo seletivo.

## Situação atual

Atualmente esta disponivel no site da Novatics o e-mail pessoas@novatics.com.br para o envio de CV's.

O processo de triagem e seleção dos candidatos e feito por e-mail.

## Objetivo

Realizar uma pesquisada de mercado, para saber como as empresas estão publicando as suas informações sobre vagas disponiveis é de como e feito esse processo de triagem dos curriculos.

## Pesquisa


## Empresas

Startups:

* NuBank
* IFood
* Uber
* QuintoAndar

Software Studios:

* AIS Digital

---

### NuBank

Nubank é uma empresa startup brasileira pioneira no segmento de serviços financeiros, atuando como operadora de cartões de crédito e fintech com operações no Brasil, sediada em São Paulo e fundada em 6 de maio de 2013 por David Vélez.

Vagas: [NuBank Vagas](https://nubank.workable.com/)
Ferramenta: [Workable](https://www.workable.com/)

Possui um formulario com relacionadas a cultura da pessoa e familiaridade com questioes tecnicas relacionadas a cada uma das vagas. Possui videos com funcionariores apresentando a empresa é cada um dos cagos.

##### Informações

* Informações sobre a empresa
* Informações sobre a vaga
* Desafios para a vaga
* Requisitos para a vaga
* Beneficios da empresa

##### Formulario

* Dados pessoais(Nome, Email, Telefone)
* Carta de apresentação
* Porque quer trabalhar no NuBank
* Fale sobre coisas que voce ja fez no seu trabalho

##### Questionario

(Marcar de 1 a 5 no nivel de familiaridade)
* Qual sua familiaridade com testes automatizados
* Qual sua familiaridade com microserviços
* Qual sua familiaridade com programação funcional
* Qual sua familiaridade com integração continua
* Como voce costuma aprender coisas novas?
* Como voce conheceu o NuBank?

---

### iFood/Movile

iFood é uma empresa brasileira fundada em 2011, atuante no ramo de entrega de comida pela internet, sendo líder no setor na América Latina, com presença na Argentina, México e na Colômbia.

Vagas: [agas](https://jobs.kenoby.com/ifood)
Ferramenta: [Kenoby](http://www.kenoby.com/)

Possui uma pagina pesquisada com informações sobre a empresa e uma lista com as vagas disponiveis no momento. Possui videos com funcionarios apresentando a empresa.

##### Informações

* Informações sobre a empresa
* Atividades executadas
* Requisitos
* Beneficios 
* Localização

##### Formulario

* Informações pessoais(Nome, Email)
* Indicação
* Redes Sociais
* Informações de graduação
* Anexo do CV

##### Questonario

* Como voce conheceu a empresa
* Como ficou sabendo da vaga
* Caracteristica sobre o grupo Movile

---

### QuintoAndar

QuintoAndar é uma startup brasileira de tecnologia focada no aluguel de imóveis.

Vagas: [QuintoAndar Vagas](https://quintoandar.workable.com/)
Ferramenta: [Ferramenta](http://workable.com)

Possui informações bem claras sobre o processo seletivo, bem proximo ao candidato.

##### Informações

* Informações sobre a empresa
* Informações sobre a cultura
* Infromaçẽos sobre os passos do processo seletivo

##### Formulario

* Informações pessoais(Nome, Email, Telefone, FOTO)
* Informações sobre experiencias profisionais anteriores

##### Questonario

* Porquer quer trabalhar aqui?
* Porque devemos contrata-lo?
* Como ficou sabendo da oportunidade?
* Conhece alguem que trabalha aqui?

---

### AIS Digital

Cria produtos digitais por demanda

Vagas: [Vagas](https://www.aisdigital.com.br/team)
Ferramenta: [Ferramenta](https://www.bamboohr.com/)

Possui informações basicas sobre as diferentes vagas, não realiza questionario tecnico sobre a vaga, mas e transparente com relação aos passos do processo seletivo.

##### Informações

* Informações da Empresa
* Atividades a serem executadas
* Skils importantes
* Beneficios

##### Formulario

* Informações Pessoais
* CV
* Links para Portigolio
* Como soube da vaga

##### Questonario

N/A



---

### Empresa

Texto sobre a empresa

Vagas: [Empresa Vagas]()
Ferramenta: [Ferramenta]()

Uma breve descrição sobre o processo seletivo

##### Informações

* Lista

##### Formulario

* Lista

##### Questonario

* Lista


